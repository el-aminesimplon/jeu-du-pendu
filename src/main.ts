//mot a trouvé

let tableau = [
  "herisson","coquelicot","pamplemousse","caoutchouc","pediatre","fleau","haricot",
  "pansement","fourrure","plombier","grappin","gobelet","pinceau","torchon","ongle",
  "fourriere","bienveillant","horizon","inoxydable","immonde","parchemin"
];

// Creation en tableau du mot car il doit etre divisé en tableau lettre par lettre
let tab = Math.floor(Math.random() * tableau.length);
let tableauDeMot = tableau[tab];

let tableauDeLettreASupprimer = tableauDeMot.split("");

let tableauLettresTrouvees:string[] = [];

let tableauDeMotInitial = tableauDeMot.split("");

let tentative = 9;

let motAAfficher = "";

for (const lettre of tableauDeMotInitial) {
  lettre;
  motAAfficher += "_ ";
}

const motATrouver = document.querySelector<HTMLElement>("#motATrouver");

motATrouver.innerHTML = `Mot a trouver: ${motAAfficher}`;


// On recupere l'element input
// On Ajoute sur cet element le keyup

const inputSaisie = document.querySelector<HTMLInputElement>("#input");
const form = document.querySelector<HTMLInputElement>("form");
const visual = document.querySelector<HTMLInputElement>(".visual");
const affichageDuSaisie = document.querySelector<HTMLElement>("h3");



form.addEventListener("keyup", () => {

  let valeurSaisie = inputSaisie.value;
    affichageDuSaisie.innerHTML +=inputSaisie.value.toUpperCase()+" ";


  let nombDeLettreTrouver = 0;

  while (tableauDeLettreASupprimer.indexOf(valeurSaisie) != -1) {
    let indice = tableauDeLettreASupprimer.indexOf(valeurSaisie);
    tableauDeLettreASupprimer.splice(indice, 1);
    tableauLettresTrouvees.push(valeurSaisie);
    nombDeLettreTrouver += 1;
  }

  inputSaisie.value = "";

  const perdu = document.querySelector<HTMLElement>("#perdu");
  let images = document.querySelector(".img");



  if (nombDeLettreTrouver == 0) {
    tentative += -1;



    let urlImage = "";

    switch (tentative) {
      case 8:
        urlImage = "/conteneur/1.jpg";
        break;

      case 7:
        urlImage = "/conteneur/2.jpg";
        break;

      case 6:
        urlImage = "/conteneur/3.jpg";
        break;

      case 5:
        urlImage = "/conteneur/4.jpg";
        break;

      case 4:
        urlImage = "/conteneur/5.jpg";
        break;

      case 3:
        urlImage = "/conteneur/6.jpg";
        break;

      case 2:
        urlImage = "/conteneur/7.jpg";
        break;

      case 1:
        urlImage = "/conteneur/8.jpg";
        break;

      default: urlImage = "/conteneur/9.jpg";
        perdu.innerHTML = "Vous avez perdu!"
        inputSaisie.style.display = "none";
        break;
    }

    images.setAttribute("src", urlImage);

    let chance = document.querySelector<HTMLElement>("#chance");
    chance.innerHTML = `Il vous reste ${tentative} tentative.`

  }

  inputSaisie.innerHTML = "";
  motAAfficher = "";

  for (const itemLettre of tableauDeMotInitial) {

    if (tableauLettresTrouvees != null && tableauLettresTrouvees.indexOf(itemLettre) != -1) {
      motAAfficher += itemLettre;
    } else {
      motAAfficher += "_ ";
    }
  }


  motATrouver.innerHTML = `Mot à trouver: ${motAAfficher}`;


  if (motAAfficher.length == tableauDeMotInitial.length) {
    
    motATrouver.innerHTML = `Le mot est bien: ${tableauDeMot.toUpperCase()}`;

    let gagner = document.createElement("h1");
    visual.append(gagner)
    gagner.style.backgroundColor = "green"
    gagner.innerHTML = "Bien joué!"

    inputSaisie.style.display = "none";

    images.setAttribute("src", "https://gifdb.com/images/high/penguins-win-jump-x2ftytqan4s23qrb.gif");
    images.classList.add("big");

  }
  if( tentative <1){
    
    motATrouver.innerHTML = `Le mot était: ${tableauDeMot.toUpperCase()}`;
  }
});